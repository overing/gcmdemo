
document.addEventListener('DOMContentLoaded', function () {
    chrome.pushMessaging.getChannelId(true, function(e) {
        var urlQRCode = 'http://chart.apis.google.com/chart';
        urlQRCode += '?cht=qr';
        urlQRCode += '&chs=120x120';
        urlQRCode += '&chl=' + encodeURI(e.channelId);

        $('div#qrc').append($('<img/>').attr({
            'src': urlQRCode, 
            'alt': urlQRCode
        }));

        $('input#snd').click(function(e) {
            var rid = $('input#rid').prop('value');
            var msg = $('input#msg').prop('value');
            if (rid && msg) {
                $.ajax({
                    type: 'POST', 
                    url: 'https://android.googleapis.com/gcm/send', 
                    headers: {
                        'Authorization': 'key=AIzaSyBvPXk9vDoAVbfUfJ3UuD6sxVR-X0ia14c'
                    }, 
                    contentType: 'application/json', 
                    data: JSON.stringify({
                        'collapse_key': '0', 
                        'registration_ids': [ rid ], 
                        'delay_while_idle': true, 
                        'data': {
                            'message': msg
                        }
                    }), 
                    dataType: 'json'
                })
                .done(function(e) {
                    $('div#result').text(JSON.stringify(e));
                })
                .fail(function(xhr, e, err) {
                    $('div#result').text(e + ": " + err);
                });
            }
        });
    });

    chrome.pushMessaging.onMessage.addListener(function (e) {
        $('input#rid').attr('value', e.payload);
    });
});
package org.overwork.gcmdemo;

import com.google.android.gcm.GCMRegistrar;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

public class GCMDemo extends Application {
	
	private static final String TAG = GCMDemo.class.getSimpleName();
	private static final String SENDER_ID = "387808156900";

	@Override
	public void onCreate() {
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		final String regId = GCMRegistrar.getRegistrationId(this);
		if (TextUtils.isEmpty(regId)) {
			GCMRegistrar.register(this, SENDER_ID);
		} else {
			Log.w(TAG, "GCM register id: " + regId);
		}
	}
}

package org.overwork.gcmdemo.content;

import static org.overwork.gcmdemo.content.GCMDB.*;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class GCMDBOpenHelper extends SQLiteOpenHelper {

    public GCMDBOpenHelper(Context context, String name) {
        super(context, name, null, SCHEMA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + Message.RESOURCE_NAME + 
            " (" + Message._ID +     " INTEGER PRIMARY KEY AUTOINCREMENT" + 
            ", " + Message.DATE +    " INTEGER" + 
            ", " + Message.CONTENT + " TEXT" + 
        ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Message.RESOURCE_NAME);

        onCreate(db);
    }
}

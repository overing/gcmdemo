package org.overwork.gcmdemo.content;

import static org.overwork.gcmdemo.content.GCMDB.*;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.SparseArray;

public class GCMDBProvider extends ContentProvider {

    private static final int MESSAGE_ID       = 0x00010001;
    private static final int MESSAGES         = 0x0001FFFF;

    private static final UriMatcher sUriMatcher;
    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        sUriMatcher.addURI(AUTHORITY, Message.RESOURCE_NAME + "/#", MESSAGE_ID);
        sUriMatcher.addURI(AUTHORITY, Message.RESOURCE_NAME, MESSAGES);
    }

    private static final SparseArray<String> sCodeToPath;
    static {
        sCodeToPath = new SparseArray<String>();

        sCodeToPath.put(MESSAGE_ID, Message.RESOURCE_NAME);
        sCodeToPath.put(MESSAGES, Message.RESOURCE_NAME);
    }

    public static String compiledWhereClause(String selection, String idColumn) {
        return TextUtils.isEmpty(selection) ? String.format("%s=?", idColumn)
                : String.format("%s %s=?", selection, idColumn);
    }

    public static String[] compiledWhereArgs(long _id, String selection, String[] selectionArgs) {
        String[] args = new String[] { String.valueOf(_id) };
        if (!TextUtils.isEmpty(selection) && selectionArgs != null) {
            args = new String[selectionArgs.length + 1];
            for (int i = 0; i < selectionArgs.length; i++) {
                args[i] = selectionArgs[i];
            }
            args[selectionArgs.length] = String.valueOf(_id);
        }
        return args;
    }

    private GCMDBOpenHelper mOpenHelper;

    @Override
    public boolean onCreate() {
        mOpenHelper = new GCMDBOpenHelper(getContext(), CONTENT_NAME);
        return true;
    }

    @Override
    public String getType(Uri uri) {
        final String DIR_PIX = ContentResolver.CURSOR_DIR_BASE_TYPE;
        final String ITEM_PIX = ContentResolver.CURSOR_ITEM_BASE_TYPE;

        int code = sUriMatcher.match(uri);
        if (code != -1) {
            boolean dir = (code & 0x0000FFFF) == 0x0000FFFF;
            String path = sCodeToPath.get(code);
            return (dir ? DIR_PIX : ITEM_PIX) + "/" + CONTENT_NAME + "." + path;
        } else {
            return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long _id;
        Uri rval = null;

        int code = sUriMatcher.match(uri);
        if (code != -1) {
            _id = db.insert(sCodeToPath.get(code), null, values);
            rval = ContentUris.withAppendedId(uri, _id);
        }

        if (rval != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rval;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long _id;
        String whereClause, whereArgs[];
        int rval = 0;

        int code = sUriMatcher.match(uri);
        if (code != -1) {
            boolean dir = (code & 0x0000FFFF) == 0x0000FFFF;
            String path = sCodeToPath.get(code);
            if (dir) {
                rval = db.delete(path, selection, selectionArgs);
            } else {
                _id = ContentUris.parseId(uri);
                whereClause = compiledWhereClause(selection, BaseColumns._ID);
                whereArgs = compiledWhereArgs(_id, selection, selectionArgs);
                rval = db.delete(path, whereClause, whereArgs);
            }
        }

        if (rval > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rval;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long _id;
        String whereClause, whereArgs[];
        int rval = 0;

        int code = sUriMatcher.match(uri);
        if (code != -1) {
            boolean dir = (code & 0x0000FFFF) == 0x0000FFFF;
            String path = sCodeToPath.get(code);
            if (dir) {
                rval = db.update(path, values, selection, selectionArgs);
            } else {
                _id = ContentUris.parseId(uri);
                whereClause = compiledWhereClause(selection, BaseColumns._ID);
                whereArgs = compiledWhereArgs(_id, selection, selectionArgs);
                rval = db.update(path, values, whereClause, whereArgs);
            }
        }

        if (rval > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rval;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        long _id;
        String whereClause, whereArgs[];
        Cursor rval = null;

        int code = sUriMatcher.match(uri);
        if (code != -1) {
            boolean dir = (code & 0x0000FFFF) == 0x0000FFFF;
            String path = sCodeToPath.get(code);
            if (dir) {
                rval = db.query(path, projection, selection, selectionArgs, null, null, sortOrder);
            } else {
                _id = ContentUris.parseId(uri);
                whereClause = compiledWhereClause(selection, BaseColumns._ID);
                whereArgs = compiledWhereArgs(_id, selection, selectionArgs);
                rval = db.query(path, projection, whereClause, whereArgs, null, null, sortOrder);
            }
        }

        if (rval != null) {
            rval.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return rval;
    }
}

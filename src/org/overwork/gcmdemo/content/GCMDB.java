package org.overwork.gcmdemo.content;

import android.net.Uri;
import android.provider.BaseColumns;

public class GCMDB {

	public static final int SCHEMA_VERSION = 1;

	public static final String AUTHORITY = "org.overwork.gcmdemo.content";

	public static final String CONTENT_NAME = "gcm_demo_database";

	public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

	public static class Message implements BaseColumns {

		public static final String CONTENT = "content";

		public static final String DATE = "date";

		public static final String RESOURCE_NAME = "message";

		public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, RESOURCE_NAME);

		private Message() {
		}
	}
}

package org.overwork.gcmdemo;

import org.overwork.gcmdemo.R;
import org.overwork.gcmdemo.content.GCMDB;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = GCMIntentService.class.getSimpleName();

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.d(TAG, "onMessage");

		Bundle bundle = intent.getExtras();
		for (String key : bundle.keySet()) {
		    Log.w("M", key + "='" + bundle.get(key) + "'");
		}
		
		String message = intent.getStringExtra("message");
		generateNotification(context, message);
		writeToDB(context, message);
	}

	@Override
	protected void onError(Context context, String errorId) {
		Log.d(TAG, "onError id: " + errorId);
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.d(TAG, "onRegistered id: " + registrationId);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.d(TAG, "onUnregistered id: " + registrationId);
	}

	private void generateNotification(Context context, String message) {
        Intent main = new Intent(context, MainActivity.class);
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(0, new Notification.Builder(context)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(message)
                .setContentIntent(PendingIntent.getActivity(context, 0, main, 0))
                .build());
	}

	private void writeToDB(Context context, String message) {
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues(2);
		values.put(GCMDB.Message.CONTENT, message);
		values.put(GCMDB.Message.DATE, System.currentTimeMillis());
		cr.insert(GCMDB.Message.CONTENT_URI, values);
	}
}

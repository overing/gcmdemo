package org.overwork.gcmdemo;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.overwork.gcmdemo.content.GCMDB;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends ListActivity //
        implements LoaderManager.LoaderCallbacks<Cursor>, //
        SimpleCursorAdapter.ViewBinder, View.OnClickListener {

    private static final int LOADER_ID_MESSAGE = 0;

    private static final String DATE_FORAMT = "yyyy/MM/dd hh:mm:ss aa";

    private final Context context = MainActivity.this;

    private AndroidHttpClient mHttpClient;

    private SimpleCursorAdapter mAdapter;

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
        case LOADER_ID_MESSAGE:
            Uri contentUri = GCMDB.Message.CONTENT_URI;
            return new CursorLoader(this, contentUri, null, null, null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
        if (cursor.getColumnIndex(GCMDB.Message.DATE) == columnIndex) {
            long datetime = cursor.getLong(columnIndex);
            TextView text = (TextView) view.findViewById(android.R.id.text2);
            text.setText(DateFormat.format(DATE_FORAMT, datetime));
            return true;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_match_sender) {
            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.initiateScan(IntentIntegrator.QR_CODE_TYPES);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHttpClient = AndroidHttpClient.newInstance(getString(R.string.app_name), this);

        setContentView(R.layout.activity_main);

        int layout = android.R.layout.simple_list_item_2;
        String[] from = { GCMDB.Message.CONTENT, GCMDB.Message.DATE };
        int[] to = { android.R.id.text1, android.R.id.text2 };
        int flags = SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER;
        mAdapter = new SimpleCursorAdapter(this, layout, null, from, to, flags);
        mAdapter.setViewBinder(this);

        setListAdapter(mAdapter);
        getListView().setEmptyView(findViewById(android.R.id.empty));

        findViewById(R.id.btn_match_sender).setOnClickListener(this);

        getLoaderManager().initLoader(LOADER_ID_MESSAGE, null, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,
                resultCode, data);
        if (result != null) {
            String chromeChannelId = result.getContents();
            new SendRIDTask().execute(chromeChannelId);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public class SendRIDTask extends AsyncTask<String, Void, String> {

        ProgressDialog mPD;
        Exception mEX;

        @Override
        protected void onPreExecute() {
            String message = "Send registration id...";
            mPD = ProgressDialog.show(context, null, message, false, false);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject json = new JSONObject();
                json.put("channelId", params[0]);
                json.put("subchannelId", "0");
                json.put("payload", GCMRegistrar.getRegistrationId(context));
                HttpPost post = new HttpPost("https://www.googleapis.com/gcm_for_chrome/v1/messages");
                post.setHeader("Authorization", "Bearer ya29.AHES6ZQUX6qDus3m5Ren_4238sB8Zhx4ex2n6A5qwFZLzIZt");
                post.setHeader("Content-Type", "application/json");
                post.setEntity(new StringEntity(json.toString(), "US-ASCII"));

                HttpResponse respance = mHttpClient.execute(post);
                return respance.getEntity() == null ? "" : EntityUtils.toString(respance.getEntity());
            } catch (Exception ex) {
                Log.e("Main", "on send registration id to chrome fail", ex);
                mEX = ex;
                return null;
            }
        }
        
        @Override
        protected void onPostExecute(String result) {
            mPD.dismiss();
            if (mEX == null) {
                Log.w("Main", result);
            } else {
                AlertDialog.Builder adb = new AlertDialog.Builder(context);
                adb.setTitle("Send registration id fail");
                adb.setMessage(mEX.getMessage());
                adb.setPositiveButton(android.R.string.ok, null);
                adb.show();
            }
        }
    }
}
